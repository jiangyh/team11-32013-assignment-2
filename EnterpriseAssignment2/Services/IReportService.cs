
﻿using EnterpriseAssignment2.Models;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Web;
using System.Data.Entity;
using System.Web.Mvc;


namespace EnterpriseAssignment2.Services
{
    public interface IReportService
    {
        UserInfo FindUserById(string id);
        DbSet<ClientType> GetClientTypeSet();
        IQueryable<District> GetDistrictByUser(IPrincipal user);
        DbSet<District> GetDistrictSet();
        decimal GetTotalCost();
        decimal GetTotalLabor();
        IQueryable<ListCostsbyEngineerViewModel> ListAverageCostsbyEngineer();
        IQueryable<ListCostsbyDistrictViewModel> ListCostsbyDistrict();
        IQueryable<UserInfo> ListEngineersAndManagers();
        IQueryable<MontlyCostbyDistrictModel> ListMonthlyCostsbyDistrict(int districtId);
        IQueryable<ListCostsbyEngineerViewModel> ListTotalCostsbyEngineer();
        void UpdateUserInfo(UserInfo userInfo);
    }
}