﻿using EnterpriseAssignment2.Models;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Web;
using System.Data.Entity;
using System.Web.Mvc;

namespace EnterpriseAssignment2.Services
{
    public class ReportService : IReportService
    {
        private ENET_CARE_Entities db;
        private const string COMPLETE = "Completed";

        public ReportService()
        {
            db = new ENET_CARE_Entities();
        }

        public IQueryable<ListCostsbyEngineerViewModel> ListTotalCostsbyEngineer()
        {
            var report = db.Intervention.Where(w => w.State.Equals(COMPLETE))
                                        .GroupBy(o => o.ProposingUserObject.UserName)
                                        .Select(g => new ListCostsbyEngineerViewModel
                                                {
                                                    UserName = g.Key,
                                                    Cost = g.Sum(i => i.Cost),
                                                    Labor = g.Sum(i => i.LabourRequired)
                                                })
                                        .OrderBy(f => f.UserName);

            return report;

        }

        public IQueryable<ListCostsbyEngineerViewModel> ListAverageCostsbyEngineer()
        {
            var report = db.Intervention.Where(w => w.State.Equals(COMPLETE))
                                        .GroupBy(o => o.ProposingUserObject.UserName)
                                        .Select(g => new ListCostsbyEngineerViewModel
                                        {
                                            UserName = g.Key,
                                            Cost = g.Average(i => i.Cost),
                                            Labor = g.Average(i => i.LabourRequired)
                                        })
                                        .OrderBy(f => f.UserName);

            return report;

        }

        public IQueryable<ListCostsbyDistrictViewModel> ListCostsbyDistrict()
        {
            var report = db.Intervention.Where(w => w.State.Equals(COMPLETE))
                                        .GroupBy(o => o.Client.District.Name)
                                        .Select(g => new ListCostsbyDistrictViewModel
                                        {
                                            DistrictName = g.Key,
                                            Cost = g.Sum(i => i.Cost),
                                            Labor = g.Sum(i => i.LabourRequired)
                                        })
                                        .OrderBy(f => f.DistrictName);

            return report;

        }

        public decimal GetTotalCost()
        {
            decimal totalCost = db.Intervention.Where(w => w.State.Equals(COMPLETE))
                                               .Sum(i => i.Cost);
            return totalCost;                                   
        }

        public decimal GetTotalLabor()
        {
            decimal totalLabor = db.Intervention.Where(w => w.State.Equals(COMPLETE))
                                               .Sum(i => i.LabourRequired);
            return totalLabor;
        }


        public IQueryable<MontlyCostbyDistrictModel> ListMonthlyCostsbyDistrict(int districtId)
        {
            var report = db.Intervention.Where(w => w.State.Equals(COMPLETE))
                                        .Where(w => w.Client.DistrictId == districtId)
                                        .GroupBy(o => new YearWithMonthModel { Year = o.PerformedDate.Value.Year, Month = o.PerformedDate.Value.Month })
                                        .Select(g => new MontlyCostbyDistrictModel
                                        {
                                            YearWithMonth = g.Key,
                                            Cost = g.Sum(i => i.Cost),
                                            Labor = g.Sum(i => i.LabourRequired)
                                        })
                                        .OrderBy(f => f.YearWithMonth.Year).ThenBy(f => f.YearWithMonth.Month);

            return report;

        }

        public IQueryable<UserInfo> ListEngineersAndManagers()
        {
            return db.UserInfo.Include(u => u.AspNetUsers).Include(u => u.District);
        }

        public UserInfo FindUserById(string id)
        {
            return db.UserInfo.Find(id);
        }

        public void UpdateUserInfo(UserInfo userInfo)
        {
            db.Entry(userInfo).State = EntityState.Modified;
            db.SaveChanges();
        }


        // Utility method
        public IQueryable<District> GetDistrictByUser(IPrincipal user)
        {
            var currentDistrictId = db.UserInfo.Find(user.Identity.GetUserId()).ResponsibleDistrict;
            return db.District.Where(d => d.Id == currentDistrictId);
        }
        public DbSet<District> GetDistrictSet()
        {
            return db.District;
        }
        public DbSet<ClientType> GetClientTypeSet()
        {
            return db.ClientType;
        }

    }
}