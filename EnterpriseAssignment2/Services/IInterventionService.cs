﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Security.Principal;
using EnterpriseAssignment2.Models;

namespace EnterpriseAssignment2.Services
{
    public interface IInterventionService
    {
        bool Approve(Intervention intervention, IPrincipal user);
        void ChangeQualityInformation(Intervention intervention);
        void CreateIntervention(Intervention intervention, IPrincipal proposingUser);
        void DeleteInterventionById(int? id);
        Intervention FindInterventionById(int? id);
        IEnumerable<AspNetUsers> GetAspNetUsersSet();
        IQueryable<Client> GetClientById(int? id);
        IEnumerable<Client> GetClientSet();
        IEnumerable<InterventionType> GetInterventionTypeSet();
        List<Intervention> ListInterventionsByClientId(int? id);
        List<Intervention> ListInterventionsByEngineer(IPrincipal user);
        List<Intervention> ListInterventionsByManager(IPrincipal user);
        void UpdateIntervention(Intervention intervention);
        void UpdateInterventionStateToCanceled(int? id);
        InterventionType FindInterventionType(int? id);
    }
}