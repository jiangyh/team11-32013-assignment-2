﻿using EnterpriseAssignment2.Models;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Web;
using System.Data.Entity;
using System.Web.Mvc;

namespace EnterpriseAssignment2.Services
{
    public class ClientService : IClientService
    {
        private ENET_CARE_Entities db;

        public ClientService()
        {
            db = new ENET_CARE_Entities();
        }

        public List<Client> FindClientsByUserDistrict(IPrincipal user)
        {
            var currentUserId = user.Identity.GetUserId();
            var currentDistrict = db.UserInfo.Find(currentUserId).ResponsibleDistrict;
            var clients = db.Client.Include(c => c.District)
                                  .Include(c => c.ClientType)
                                  .Where(c => c.DistrictId == currentDistrict)
                                  .ToList();
            return clients;
        }

        public Client FindClientById(int? id)
        {
            return db.Client.Find(id);
        }


        public void CreateClient(Client client)
        {
            db.Client.Add(client);
            db.SaveChanges();
        }

        // Utility method
        public IQueryable<District> GetDistrictByUser(IPrincipal user)
        {
            var currentDistrictId = db.UserInfo.Find(user.Identity.GetUserId()).ResponsibleDistrict;
            return db.District.Where(d => d.Id == currentDistrictId);
        }
        public IEnumerable<District> GetDistrictSet()
        {
            return db.District;
        }
        public IEnumerable<ClientType> GetClientTypeSet()
        {
            return db.ClientType;
        }

    }
}