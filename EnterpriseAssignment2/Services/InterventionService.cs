﻿using EnterpriseAssignment2.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Security.Principal;
using System.Data;
using System.Data.Entity;
using System.Net;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace EnterpriseAssignment2.Services
{
    public class InterventionService : IInterventionService
    {
        private ENET_CARE_Entities db;

        public InterventionService()
        {
            db = new ENET_CARE_Entities();
        }

        public List<Intervention> ListInterventionsByClientId(int? id)
        {
            var interventions = db.Intervention.Include(i => i.ProposingUserObject)
                                              .Include(i => i.ApprovalUserObject)
                                              .Include(i => i.InterventionType)
                                              .Where(i => i.ClientId == id && !i.State.Equals("Cancelled"))
                                              .ToList();
            return interventions;
        }

        public List<Intervention> ListInterventionsByEngineer(IPrincipal user)
        {
            string currentUserName = user.Identity.GetUserId();
            var interventions = db.Intervention.Include(i => i.ProposingUserObject)
                                              .Include(i => i.ApprovalUserObject)
                                              .Include(i => i.Client)
                                              .Include(i => i.InterventionType)
                                              .Where(i => i.ProposingUser == currentUserName && !i.State.Equals("Cancelled"))
                                              .ToList();
            return interventions;
        }

        public List<Intervention> ListInterventionsByManager(IPrincipal user)
        {
            var currentUserInfo = db.UserInfo.Find(user.Identity.GetUserId());
            var currentUserDistrictId = currentUserInfo.ResponsibleDistrict;
            var userCost = currentUserInfo.ApprovableCost;
            var userLabour = currentUserInfo.ApprovableLabour;
            var interventions = db.Intervention.Include(i => i.ProposingUserObject)
                                              .Include(i => i.ApprovalUserObject)
                                              .Include(i => i.Client)
                                              .Include(i => i.InterventionType)
                                              .Where(i => i.State == "Proposed" &&
                                                     i.Client.District.Id == currentUserDistrictId &&
                                                     !i.State.Equals("Cancelled"))
                                              .Where(i => i.Cost <= userCost)
                                              .Where(i => i.LabourRequired <= userLabour)
                                              .ToList();
            return interventions;
        }

        public Intervention FindInterventionById(int? id)
        {
            var intervention = db.Intervention.Find(id);
            return intervention;
        }

        public void UpdateIntervention(Intervention intervention)
        {
            var currentIntervention = db.Intervention.Find(intervention.InterventionId);
            currentIntervention.RemainingLife = intervention.RemainingLife;
            currentIntervention.Note = intervention.Note;
            db.Entry(currentIntervention).State = EntityState.Modified;
            db.SaveChanges();
        }

        public void UpdateInterventionStateToCanceled(int? id)
        {
            var intervention = db.Intervention.Find(id);
            intervention.State = "Cancelled";
            db.Entry(intervention).State = EntityState.Modified;
            db.SaveChanges();
        }

        public void DeleteInterventionById(int? id)
        {
            var intervention = db.Intervention.Find(id);
            db.Intervention.Remove(intervention);
            db.SaveChanges();
        }

        public void CreateIntervention(Intervention intervention, IPrincipal proposingUser)
        {
            var currentUserId = proposingUser.Identity.GetUserId();
            var currentUser = db.AspNetUsers.Include(u => u.UserInfo).SingleOrDefault(x => x.Id == currentUserId);
            intervention.ProposingUser = proposingUser.Identity.GetUserId();
            intervention.PerformedDate = DateTime.Now;
            intervention.RecentVisitDate = DateTime.Now;
            intervention.RemainingLife = 100;

            var currentInterventionType = db.InterventionType.Find(intervention.Type);
            if (currentUser.UserInfo.ApprovableCost >= intervention.Cost &&
                currentUser.UserInfo.ApprovableLabour >= intervention.LabourRequired &&
                currentUser.UserInfo.ApprovableCost >= currentInterventionType.DefaultCost &&
                currentUser.UserInfo.ApprovableLabour >= currentInterventionType.DefaultLabour)
            {
                intervention.State = "Completed";
                intervention.ApprovalUser = currentUser.Id;
            }
            else
            {
                intervention.State = "Proposed";
                intervention.ApprovalUser = null;
            }

            db.Intervention.Add(intervention);
            db.SaveChanges();
        }

        public void ChangeQualityInformation(Intervention intervention)
        {

        }

        public bool Approve(Intervention intervention, IPrincipal user)
        {
            UserInfo currentUser = db.UserInfo.Find(user.Identity.GetUserId());
            if (currentUser.ApprovableCost >= intervention.Cost && currentUser.ApprovableLabour >= intervention.LabourRequired)
            {
                intervention.State = "Approved";
                db.Entry(intervention).State = EntityState.Modified;
                db.SaveChanges();
                return true;
            }
            else
            {
                return false;
            }
        }


        // Utility method
        public IEnumerable<AspNetUsers> GetAspNetUsersSet()
        {
            return db.AspNetUsers;
        }
        public IEnumerable<InterventionType> GetInterventionTypeSet()
        {
            return db.InterventionType;
        }
        public IEnumerable<Client> GetClientSet()
        {
            return db.Client;
        }
        public IQueryable<Client> GetClientById(int? id)
        {
            return db.Client.Where(c => c.ClientId == id);
        }

        public InterventionType FindInterventionType(int? id)
        {
            return db.InterventionType.Find(id);
        }
    }
}