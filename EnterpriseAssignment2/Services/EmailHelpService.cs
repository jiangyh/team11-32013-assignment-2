﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using EnterpriseAssignment2.Models;
using System.Net;
using System.Net.Mail;
using System.Security.Principal;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System.Threading.Tasks;

namespace EnterpriseAssignment2.Services
{
    public class EmailHelpService : Controller
    {
        private ENET_CARE_Entities db;

        public EmailHelpService()
        {
            db = new ENET_CARE_Entities();
        }

        public async Task<ActionResult> SendEmail(string currentState, int? id, string nextPage)
        {
            var model = new EmailFormModel();
            var currentUserEmail = db.AspNetUsers.Find(db.Intervention.Find(id).ProposingUser).Email;
            var body = "<p>Email From: ENETCare </p><p>Message:</p><p>Your intervention state has changed to " + currentState + "</p>";
            var message = new MailMessage();
            message.To.Add(new MailAddress(currentUserEmail));  // replace with valid value 
            message.From = new MailAddress("outlook_930258E6902302ED@outlook.com");  // replace with valid value
            message.Subject = "Your intervention state has changed to " + currentState;
            message.Body = string.Format(body, model.FromName, model.FromEmail, model.Message);
            message.IsBodyHtml = true;

            using (var smtp = new SmtpClient())
            {
                var credential = new NetworkCredential
                {
                    //UserName = "enentcare11@gmail.com",  // replace with valid value
                    //Password = "32013.NET"  // replace with valid value
                    UserName = "wwww09z@sina.com",  // replace with valid value
                    Password = "32013.NET"  // replace with valid value
                };
                smtp.Credentials = credential;
                smtp.Host = "smtp-mail.outlook.com";
                smtp.Port = 587;
                smtp.EnableSsl = true;
                await smtp.SendMailAsync(message);
                return RedirectToAction(nextPage);
            }
        }
    }

}