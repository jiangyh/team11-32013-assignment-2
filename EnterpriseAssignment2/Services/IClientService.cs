﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Security.Principal;
using EnterpriseAssignment2.Models;

namespace EnterpriseAssignment2.Services
{
    public interface IClientService
    {
        void CreateClient(Client client);
        Client FindClientById(int? id);
        List<Client> FindClientsByUserDistrict(IPrincipal user);
        IEnumerable<ClientType> GetClientTypeSet();
        IQueryable<District> GetDistrictByUser(IPrincipal user);
        IEnumerable<District> GetDistrictSet();
    }
}