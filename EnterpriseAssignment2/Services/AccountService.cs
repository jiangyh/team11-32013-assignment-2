﻿using EnterpriseAssignment2.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Security.Principal;
using System.Data;
using System.Data.Entity;
using System.Net;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace EnterpriseAssignment2.Services
{
    public class AccountService
    {
        private ENET_CARE_Entities db;

        public AccountService()
        {
            db = new ENET_CARE_Entities();
        }

        public string findRole(string username)
        {
            var currentUserRole = db.AspNetUsers
                                    .Include(u => u.AspNetRoles)
                                    .SingleOrDefault(u => u.UserName == username)
                                    .AspNetRoles.ElementAt(0)
                                    .Name;
            return currentUserRole;
        }
    }
}