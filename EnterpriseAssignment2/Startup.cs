﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(EnterpriseAssignment2.Startup))]
namespace EnterpriseAssignment2
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
