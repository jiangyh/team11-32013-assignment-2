//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace EnterpriseAssignment2.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;

    public partial class Intervention
    {
        public int InterventionId { get; set; }
        [Display(Name = "Client Name")]
        public int ClientId { get; set; }
        [Display(Name = "Proposing User")]
        public string ProposingUser { get; set; }
        [Display(Name = "Approving User")]
        public string ApprovalUser { get; set; }
        public int Type { get; set; }
        [Display(Name = "Labour Required")]
        public decimal LabourRequired { get; set; }
        public decimal Cost { get; set; }
        [Display(Name = "Performed Date")]
        public Nullable<System.DateTime> PerformedDate { get; set; }
        public string State { get; set; }
        [Display(Name = "RecentVisit Date")]
        public Nullable<System.DateTime> RecentVisitDate { get; set; }
        [Display(Name = "Remaining Life")]
        public int RemainingLife { get; set; }
        public string Note { get; set; }

        [Display(Name = "Proposing User")]
        public virtual AspNetUsers ProposingUserObject { get; set; }
        [Display(Name = "Approval User")]
        public virtual AspNetUsers ApprovalUserObject { get; set; }
        [Display(Name = "Client")]
        public virtual Client Client { get; set; }
        [Display(Name = "Intervention Type")]
        public virtual InterventionType InterventionType { get; set; }
    }
}
