﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EnterpriseAssignment2.Models
{
    public class UserInformationViewModel
    {
        public string Id { get; set; }
        public string UserName { get; set; }
        public string FullName { get; set; }
        public string ResponsibleDistrictName { get; set; }
        public decimal? ApprovableCost { get; set; }
        public decimal? ApprovableLabour { get; set; }
        
    }


    public class ListCostsbyEngineerViewModel
    {
        public string UserName { get; set; }
        public decimal Cost { get; set; }
        public decimal Labor { get; set; }
    }

    public class ListCostsbyDistrictViewModel
    {
        public string DistrictName { get; set; }
        public decimal Cost { get; set; }
        public decimal Labor { get; set; }
    }

    public class ChooseDistrictViewModel
    {
        public string DistrictName { get; set;}
    }

    public class ListMontlyCostbyDistrictViewModel
    {
        public IQueryable<MontlyCostbyDistrictModel> MontlyCostbyDistrictList { get; set; }
        public District ChosenDistrict { get; set; }
    }

    public class MontlyCostbyDistrictModel
    {
        public YearWithMonthModel YearWithMonth { get; set; }
        public decimal Cost { get; set; }
        public decimal Labor { get; set; }
    }
    public class YearWithMonthModel
    {
        public int Year { get; set; }
        public int Month { get; set; }

    }
}