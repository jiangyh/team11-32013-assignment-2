﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using EnterpriseAssignment2.Models;
using EnterpriseAssignment2.Services;

namespace EnterpriseAssignment2.Controllers
{
    [Authorize(Roles = "Accountant")]
    public class ReportController : Controller
    {
        private IReportService service;

        public ReportController()
        {
            service = new ReportService();
        }
        public ReportController(IReportService mockService)
        {
            service = mockService;
        }

        // GET: Report/TotalCostsbyEngineer
        public ActionResult TotalCostsbyEngineer()
        {
            var report = service.ListTotalCostsbyEngineer();
            return View(report);
        }


        // GET: Report/AverageCostsbyEngineer
        public ActionResult AverageCostsbyEngineer()
        {
            var report = service.ListAverageCostsbyEngineer();
            return View(report);
        }

        // GET: Report/CostsByDistrict
        public ActionResult CostsbyDistrict()
        {
            var report = service.ListCostsbyDistrict();
            ViewBag.TotalCost = service.GetTotalCost();
            ViewBag.TotalLabor = service.GetTotalLabor();
            return View(report);
        }

        // GET: Report/MonthlyCostsbyDistrict
        public ActionResult MonthlyCostsbyDistrict()
        {
            var report = service.ListMonthlyCostsbyDistrict(1);
            var viewModel = new ListMontlyCostbyDistrictViewModel { MontlyCostbyDistrictList = report };
            ViewBag.ChosenDistrictId = new SelectList(service.GetDistrictSet(), "Id", "Name");
            return View(viewModel);
        }

        // POST: Report/MonthlyCostsbyDistrict
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult MonthlyCostsbyDistrict(int chosenDistrictId)
        {
            var report = service.ListMonthlyCostsbyDistrict(chosenDistrictId);
            var viewModel = new ListMontlyCostbyDistrictViewModel { MontlyCostbyDistrictList = report };
            ViewBag.ChosenDistrictId = new SelectList(service.GetDistrictSet(), "Id", "Name", chosenDistrictId);
            return View(viewModel);
        }


        // GET: UserInfo
        public ActionResult Index()
        {
            var userInfo = service.ListEngineersAndManagers();
            return View(userInfo.ToList());
        }

        // GET: UserInfo/Edit/5
        public ActionResult Edit(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            UserInfo userInfo = service.FindUserById(id);
            if (userInfo == null)
            {
                return HttpNotFound();
            }
            ViewBag.ResponsibleDistrict = new SelectList(service.GetDistrictSet(), "Id", "Name", userInfo.ResponsibleDistrict);
            return View(userInfo);
        }

        // POST: UserInfo/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,FullName,ResponsibleDistrict,ApprovableLabour,ApprovableCost")] UserInfo userInfo)
        {
            if (ModelState.IsValid)
            {
                service.UpdateUserInfo(userInfo);
                return RedirectToAction("Index");
            }
            ViewBag.ResponsibleDistrict = new SelectList(service.GetDistrictSet(), "Id", "Name", userInfo.ResponsibleDistrict);
            return View(userInfo);
        }

    }
}
