﻿using System;
using System.Data.Entity;
using System.Threading.Tasks;
using System.Net;
using System.Web.Mvc;
using EnterpriseAssignment2.Models;
using EnterpriseAssignment2.Services;


namespace EnterpriseAssignment2.Controllers
{
    [Authorize(Roles = "Manager")]
    public class ManagerController : Controller
    {
        IInterventionService service = new InterventionService();

        public ManagerController()
        {
            service = new InterventionService();
        }

        public ManagerController(IInterventionService mockService)
        {
            service = mockService;
        }

        public ActionResult Interventions()
        {
            var intervention = service.ListInterventionsByManager(User);
            return View(intervention);
        }

        public ActionResult Approve(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Intervention intervention = service.FindInterventionById(id);
            if (intervention == null)
            {
                return HttpNotFound();
            }
            return View(intervention);
        }

        [HttpPost, ActionName("Approve")]
        public async Task<ActionResult> ApproveConfirmed(int? id)
        {
            var intervention = service.FindInterventionById(id);
            var emailFormService = new EmailHelpService();
            if (service.Approve(intervention, User))
            {
                service.UpdateInterventionStateToCanceled(id);
                return await emailFormService.SendEmail("Approved", id, "Interventions");
            }
            else
            {
                return RedirectToAction("ApproveFailed/" + id);
            }
        }

        public ActionResult ApproveFailed(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Intervention intervention = service.FindInterventionById(id);
            if (intervention == null)
            {
                return HttpNotFound();
            }
            return View(intervention);
        }

        public ActionResult Cancel(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Intervention intervention = service.FindInterventionById(id);
            if (intervention == null)
            {
                return HttpNotFound();
            }
            return View(intervention);
        }

        [HttpPost, ActionName("Cancel")]
        public async Task<ActionResult> CancelConfirmed(int? id)
        {
            var emailFormService = new EmailHelpService();
            service.UpdateInterventionStateToCanceled(id);
            return await emailFormService.SendEmail("Cancel", id, "Interventions");
        }
    }
}
