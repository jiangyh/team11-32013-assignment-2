﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using EnterpriseAssignment2.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using EnterpriseAssignment2.Services;
using System.Threading.Tasks;

namespace EnterpriseAssignment2.Controllers
{
    [Authorize(Roles = "SiteEngineer")]
    public class InterventionController : Controller
    {

        IInterventionService service = new InterventionService();
        public InterventionController()
        {
            service = new InterventionService();
        }

        public InterventionController(IInterventionService mockService)
        {
            service = mockService;
        }

        // GET: Intervention
        public ActionResult List_Client(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var intervention = service.ListInterventionsByClientId(id);
            return View(intervention);
        }

        public ActionResult History()
        {
            var intervention = service.ListInterventionsByEngineer(User);
            return View(intervention);
        }


        // GET: Intervention/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Intervention intervention = service.FindInterventionById(id);
            if (intervention == null)
            {
                return HttpNotFound();
            }
            return View(intervention);
        }

        // GET: Intervention/Create
        public ActionResult Create(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ViewBag.ApprovalUser = new SelectList(service.GetAspNetUsersSet(), "Id", "Email");
            ViewBag.ProposingUser = new SelectList(service.GetAspNetUsersSet(), "Id", "Email");
            ViewBag.ClientId = new SelectList(service.GetClientById(id), "ClientId", "Name");
            ViewBag.Type = new SelectList(service.GetInterventionTypeSet(), "Id", "TypeName");
            return View();
        }

        // POST: Intervention/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "InterventionId,ClientId,ProposingUser,ApprovalUser,Type,LabourRequired,Cost,PerformedDate,State,RecentVisitDate,RemainingLife,Note")] Intervention intervention)
        {
            if (ModelState.IsValid)
            {
                var clientId = intervention.ClientId;
                service.CreateIntervention(intervention, User);
                return RedirectToAction("List_Client/" + clientId);
            }

            ViewBag.ApprovalUser = new SelectList(service.GetAspNetUsersSet(), "Id", "Email", intervention.ApprovalUser);
            ViewBag.ProposingUser = new SelectList(service.GetAspNetUsersSet(), "Id", "Email", intervention.ProposingUser);
            ViewBag.ClientId = new SelectList(service.GetClientSet(), "ClientId", "Name", intervention.ClientId);
            ViewBag.Type = new SelectList(service.GetInterventionTypeSet(), "Id", "TypeName", intervention.Type);
            return View(intervention);
        }

        // GET: Intervention/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Intervention intervention = service.FindInterventionById(id);
            intervention.RecentVisitDate = DateTime.Now;
            if (intervention == null)
            {
                return HttpNotFound();
            }
            ViewBag.ApprovalUser = new SelectList(service.GetAspNetUsersSet(), "Id", "Email", intervention.ApprovalUser);
            ViewBag.ProposingUser = new SelectList(service.GetAspNetUsersSet(), "Id", "Email", intervention.ProposingUser);
            ViewBag.ClientId = new SelectList(service.GetClientSet(), "ClientId", "Name", intervention.ClientId);
            ViewBag.Type = new SelectList(service.GetInterventionTypeSet(), "Id", "TypeName", intervention.Type);
            return View(intervention);
        }

        // POST: Intervention/Edit/5s
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "InterventionId,ClientId,ProposingUser,ApprovalUser,Type,LabourRequired,Cost,PerformedDate,State,RecentVisitDate,RemainingLife,Note")] Intervention intervention)
        {
            if (ModelState.IsValid)
            {
                service.UpdateIntervention(intervention);
                return RedirectToAction("List_Client/" + service.FindInterventionById(intervention.InterventionId).ClientId);
            }
            ViewBag.ApprovalUser = new SelectList(service.GetAspNetUsersSet(), "Id", "Email", intervention.ApprovalUser);
            ViewBag.ProposingUser = new SelectList(service.GetAspNetUsersSet(), "Id", "Email", intervention.ProposingUser);
            ViewBag.ClientId = new SelectList(service.GetClientSet(), "ClientId", "Name", intervention.ClientId);
            ViewBag.Type = new SelectList(service.GetInterventionTypeSet(), "Id", "TypeName", intervention.Type);
            return View(intervention);
        }


        public ActionResult Cancel(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Intervention intervention = service.FindInterventionById(id);
            if (intervention == null)
            {
                return HttpNotFound();
            }
            return View(intervention);
        }

        [HttpPost, ActionName("Cancel")]
        public async Task<ActionResult> CancelConfirmed(int? id)
        {
            var emailFormService = new EmailHelpService();
            
            service.UpdateInterventionStateToCanceled(id);
            return await emailFormService.SendEmail("Cancel", id, "History");
        }

        public JsonResult GetDefaultCostAndLabor(int id)
        {
            // Build a object of label values based on the selected language ID
            var result = service.FindInterventionType(id);
            var data = new
            {
                Success = true,
                Cost = result.DefaultCost,
                Label = result.DefaultLabour
            };
            return Json(data, JsonRequestBehavior.AllowGet);
        }
    }
}
