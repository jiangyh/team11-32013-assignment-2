﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using EnterpriseAssignment2.Models;
using EnterpriseAssignment2.Services;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace EnterpriseAssignment2.Controllers
{
    [Authorize(Roles = "SiteEngineer")]
    public class ClientController : Controller
    {

        private IClientService service;
        public ClientController()
        {
            service = new ClientService();
        }

        public ClientController(IClientService mockService)
        {
            service = mockService;
        }

        // GET: Client
        public ActionResult Index()
        {
            var clients = service.FindClientsByUserDistrict(User);
            return View(clients);
        }

        // GET: Client/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Client client = service.FindClientById(id);
            if (client == null)
            {
                return HttpNotFound();
            }
            return View(client);
        }

        // GET: Client/Create
        public ActionResult Create()
        {
            ViewBag.DistrictId = new SelectList(service.GetDistrictByUser(User), "Id", "Name");
            ViewBag.Type = new SelectList(service.GetClientTypeSet(), "Id", "Name");
            return View();
        }

        // POST: Client/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ClientId,Name,Type,Location,DistrictId,Contact")] Client client)
        {
            if (ModelState.IsValid)
            {
                service.CreateClient(client);
                return RedirectToAction("Index");
            }

            ViewBag.DistrictId = new SelectList(service.GetDistrictSet(), "Id", "Name", client.DistrictId);
            ViewBag.Type = new SelectList(service.GetClientTypeSet(), "Id", "Name", client.Type);
            return View(client);
        }

    }
}
