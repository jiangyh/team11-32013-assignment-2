﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using EnterpriseAssignment2;
using EnterpriseAssignment2.Controllers;
using EnterpriseAssignment2.Models;
using Moq;
using EnterpriseAssignment2.Services;
using System.Security.Principal;

namespace EnterpriseAssignment2.Tests.Service
{
    [TestClass]
    public class ClientServiceTest
    {
        [TestMethod]
        public void ClientService_ReturnView_FindClientById()
        {
            var service = new ClientService();

            var result = service.FindClientById(1);

            Assert.IsNotNull(result);
        }
    }
}
