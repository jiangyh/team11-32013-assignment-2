﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using EnterpriseAssignment2;
using EnterpriseAssignment2.Controllers;
using EnterpriseAssignment2.Models;
using Moq;
using EnterpriseAssignment2.Services;
using System.Security.Principal;

namespace EnterpriseAssignment2.Tests.Service
{
    [TestClass]
    public class ReportServiceTest
    {
        [TestMethod]
        public void ReportService_ReturnView_ListTotalCostsbyEngineer()
        {
            var service = new ReportService();

            var result = service.ListTotalCostsbyEngineer();

            Assert.IsNotNull(result);
        }

        [TestMethod]
        public void ReportService_ReturnView_ListAverageCostsbyEngineer()
        {
            var service = new ReportService();

            var result = service.ListAverageCostsbyEngineer();

            Assert.IsNotNull(result);
        }

        [TestMethod]
        public void ReportService_ReturnView_ListCostsbyDistrict()
        {
            var service = new ReportService();

            var result = service.ListCostsbyDistrict();

            Assert.IsNotNull(result);
        }

        [TestMethod]
        public void ReportService_ReturnView_GetTotalCost()
        {
            var service = new ReportService();

            var result = service.GetTotalCost();

            Assert.IsNotNull(result);
        }

        [TestMethod]
        public void ReportService_ReturnView_GetTotalLabor()
        {
            var service = new ReportService();

            var result = service.GetTotalLabor();

            Assert.IsNotNull(result);
        }

        [TestMethod]
        public void ReportService_ReturnView_ListMonthlyCostsbyDistrict()
        {
            var service = new ReportService();

            var result = service.ListMonthlyCostsbyDistrict(1);

            Assert.IsNotNull(result);
        }

        [TestMethod]
        public void ReportService_ReturnView_ListEngineersAndManagers()
        {
            var service = new ReportService();

            var result = service.ListEngineersAndManagers();

            Assert.IsNotNull(result);
        }

        [TestMethod]
        public void ReportService_ReturnView_FindUserById()
        {
            var service = new ReportService();

            var result = service.FindUserById("1030e236-4264-4e4c-8ade-77175e97fb4a");

            Assert.IsNotNull(result);
        }

        [TestMethod]
        public void ReportService_ReturnView_GetDistrictSet()
        {
            var service = new ReportService();

            var result = service.GetDistrictSet();

            Assert.IsNotNull(result);
        }

        [TestMethod]
        public void ReportService_ReturnView_GetClientTypeSet()
        {
            var service = new ReportService();

            var result = service.GetClientTypeSet();

            Assert.IsNotNull(result);
        }
    }
}
