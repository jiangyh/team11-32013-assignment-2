﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using EnterpriseAssignment2;
using EnterpriseAssignment2.Services;
using EnterpriseAssignment2.Models;
using Moq;
using System.Collections;

namespace EnterpriseAssignment2.Tests.Service
{
    [TestClass]
    public class InterventionServiceTest
    {
        [TestMethod]
        public void InterventionService_ReturnView_ListInterventionsByClientId()
        {
            var service = new InterventionService();

            var result = service.ListInterventionsByClientId(1);

            Assert.IsNotNull(result);
        }

        [TestMethod]
        public void InterventionService_ReturnView_FindInterventionById()
        {
            var service = new InterventionService();

            var result = service.FindInterventionById(1);

            Assert.IsNotNull(result);
        }

    }
}
