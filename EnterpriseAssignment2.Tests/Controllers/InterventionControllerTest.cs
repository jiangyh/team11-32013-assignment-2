﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using EnterpriseAssignment2;
using EnterpriseAssignment2.Controllers;
using EnterpriseAssignment2.Models;
using Moq;
using EnterpriseAssignment2.Services;
using System.Security.Principal;

namespace EnterpriseAssignment2.Tests.Controllers
{
    [TestClass]
    public class InterventionControllerTest
    {

        private Mock<IInterventionService> service;

        [TestInitialize]
        public void Setup()
        {
            service = new Mock<IInterventionService>();
            service.Setup(x => x.ListInterventionsByManager(It.IsAny<GenericPrincipal>()))
                   .Returns<IPrincipal>(r =>
                   {
                       return new List<Intervention>();
                   });

            service.Setup(x => x.GetAspNetUsersSet())
                   .Returns(() =>
                   {
                       List<AspNetUsers> data = new List<AspNetUsers>();
                       data.Add(new AspNetUsers());
                       return data.AsEnumerable<AspNetUsers>();
                   });

            service.Setup(x => x.GetClientSet())
                   .Returns(() =>
                   {
                       List<Client> data = new List<Client>();
                       data.Add(new Client { });
                       return data.AsEnumerable<Client>();
                   });

            service.Setup(x => x.GetInterventionTypeSet())
                   .Returns(() =>
                   {
                       List<InterventionType> data = new List<InterventionType>();
                       data.Add(new InterventionType { });
                       return data.AsEnumerable<InterventionType>();
                   });
        }

        [TestMethod]
        public void Intervention_List_ClientReturnsView()
        {
            // Arrange
            InterventionController InterventionController = new InterventionController();

            // Act
            ActionResult result = InterventionController.List_Client(1) as ViewResult;

            // Assert
            Assert.IsInstanceOfType(result, typeof(ViewResult));
        }

        [TestMethod]
        public void Intervention_List_EngReturnsView()
        {
            // Arrange
            InterventionController InterventionController = new InterventionController(service.Object);

            // Act
            ActionResult result = InterventionController.History();// as ViewResult;

            // Assert
            Assert.IsInstanceOfType(result, typeof(ViewResult));
        }


        [TestMethod]
        public void Intervention_List_ManagerReturnsView()
        {
            // Arrange
            ManagerController managerController = new ManagerController(service.Object);

            // Act
            ActionResult result = managerController.Interventions() as ViewResult;

            // Assert
            Assert.IsInstanceOfType(result, typeof(ViewResult));
        }

        [TestMethod]
        public void Intervention_DetailsReturnsView()
        {
            // Arrange
            InterventionController InterventionController = new InterventionController();

            // Act
            ActionResult result = InterventionController.Details(1) as ViewResult;

            // Assert
            Assert.IsInstanceOfType(result, typeof(ViewResult));
        }

        [TestMethod]
        public void Intervention_CreateReturnsView()
        {
            // Arrange
            InterventionController InterventionController = new InterventionController();

            // Act
            ActionResult result = InterventionController.Create(1) as ViewResult;

            // Assert
            Assert.IsInstanceOfType(result, typeof(ViewResult));
        }

        [TestMethod]
        public void Intervention_EditReturnsView()
        {
            // Arrange
            InterventionController InterventionController = new InterventionController();

            // Act
            ActionResult result = InterventionController.Edit(1) as ViewResult;

            // Assert
            Assert.IsInstanceOfType(result, typeof(ViewResult));
        }


        [TestMethod]
        public void Intervention_Post_CreateReturnsView()
        {

            service.Setup(x => x.CreateIntervention(It.IsAny<Intervention>(),It.IsAny<GenericPrincipal>()))
                   .Callback<Intervention,IPrincipal>((r,u) =>{});

            // Arrange
            InterventionController InterventionController = new InterventionController(service.Object);

            Intervention formData = new Intervention();

            // Act
            RedirectToRouteResult result = InterventionController.Create(formData) as RedirectToRouteResult;

            // Assert
            Assert.IsNotNull(result);
            Assert.IsInstanceOfType(result, typeof(RedirectToRouteResult));
        }

        [TestMethod]
        public void Intervention_Post_Validation_Failed_CreateReturnsView()
        {

            service.Setup(x => x.CreateIntervention(It.IsAny<Intervention>(), It.IsAny<GenericPrincipal>()))
                   .Callback<Intervention, IPrincipal>((r, u) => { });

            // Arrange
            InterventionController interventionController = new InterventionController(service.Object);

            Intervention formData = new Intervention();

            // Act
            RedirectToRouteResult result = interventionController.Create(formData) as RedirectToRouteResult;

            // Assert
            Assert.IsNotNull(result);
            Assert.IsInstanceOfType(result, typeof(RedirectToRouteResult));
        }


        [TestMethod]
        public void Intervention_Open_EditErrorReturnsView()
        {

            service.Setup(x => x.UpdateIntervention(It.IsAny<Intervention>()))
                   .Callback<Intervention>(r => { });

            // Arrange
            InterventionController interventionController = new InterventionController(service.Object);

            Intervention formData = new Intervention();

            // Act
            interventionController.ModelState.AddModelError("Cost", "Cost is mandatory");
            ViewResult result = interventionController.Edit(formData) as ViewResult;

            // Assert
            Assert.IsNotNull(result);
            Assert.IsInstanceOfType(result, typeof(ViewResult));
        }

    }
}
