﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using EnterpriseAssignment2;
using EnterpriseAssignment2.Controllers;
using EnterpriseAssignment2.Models;
using Moq;
using EnterpriseAssignment2.Services;
using System.Security.Principal;

namespace EnterpriseAssignment2.Tests.Controllers
{
    [TestClass]
    public class ReportControllerTest
    {
        private Mock<IReportService> service;

        [TestInitialize]
        public void Setup()
        {
            service = new Mock<IReportService>();
        }

        [TestMethod]
        public void Report_ReturnView_TotalCostsByEngineer()
        {
            ReportController controller = new ReportController();

            ViewResult result = controller.TotalCostsbyEngineer() as ViewResult;

            Assert.IsNotNull(result);
        }


        [TestMethod]
        public void Report_ReturnView_AverageCostsbyEngineer()
        {
            ReportController controller = new ReportController();

            ViewResult result = controller.AverageCostsbyEngineer() as ViewResult;

            Assert.IsNotNull(result);
        }

        [TestMethod]
        public void Report_ReturnView_CostsbyDistrict()
        {
            ReportController controller = new ReportController();

            ViewResult result = controller.CostsbyDistrict() as ViewResult;

            Assert.IsNotNull(result);
        }

        [TestMethod]
        public void Report_ReturnView_MonthlyCostsbyDistrict()
        {
            ReportController controller = new ReportController();

            ViewResult result = controller.MonthlyCostsbyDistrict() as ViewResult;

            Assert.IsNotNull(result);
        }

        [TestMethod]
        public void Report_ReturnView_MonthlyCostsby_SpecificDistrict()
        {
            ReportController controller = new ReportController();

            ViewResult result = controller.MonthlyCostsbyDistrict(1) as ViewResult;

            Assert.IsNotNull(result);
        }


        [TestMethod]
        public void Report_ReturnView_IndexTest()
        {
            ReportController controller = new ReportController();

            ViewResult result = controller.Index() as ViewResult;

            Assert.IsNotNull(result);
        }


        [TestMethod]
        public void Report_ReturnView_EditTest()
        {
            ReportController controller = new ReportController();

            ViewResult result = controller.Edit("fcb9b587-2718-48ae-880c-eb786aae8389") as ViewResult;

            Assert.IsNotNull(result);
        }

        [TestMethod]
        public void Report_ReturnView_PostEditTest()
        {

            service.Setup(x => x.UpdateUserInfo(It.IsAny<UserInfo>()))
                   .Callback<UserInfo>(r => {});

            UserInfo formData = new UserInfo
            {
                FullName = "New Full Name"
            };

            // Arrange
            ReportController controller = new ReportController(service.Object);

            // Act
            RedirectToRouteResult result = controller.Edit(formData) as RedirectToRouteResult;

            // Assert
            Assert.IsNotNull(result);
            Assert.IsInstanceOfType(result, typeof(RedirectToRouteResult));
            Assert.AreEqual("Index", result.RouteValues["Action"]);
        }

    }
}
