﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using EnterpriseAssignment2;
using EnterpriseAssignment2.Controllers;
using EnterpriseAssignment2.Models;
using Moq;
using EnterpriseAssignment2.Services;
using System.Security.Principal;

namespace EnterpriseAssignment2.Tests.Controllers
{
    [TestClass]
    public class ManagerControllerTest
    {
        private Mock<IInterventionService> service;

        [TestInitialize]
        public void Setup()
        {
            service = new Mock<IInterventionService>();
            service.Setup(x => x.ListInterventionsByManager(It.IsAny<GenericPrincipal>()))
                   .Returns<IPrincipal>(r =>
                   {
                       return new List<Intervention>();
                   });
        }

        [TestMethod]
        public void Manager_returnView_Interventions()
        {
            ManagerController controller = new ManagerController(service.Object);

            ViewResult result = controller.Interventions() as ViewResult;

            Assert.IsNotNull(result);
        }

        [TestMethod]
        public void Manager_returnView_Approve()
        {
            ManagerController controller = new ManagerController();

            ViewResult result = controller.Approve(1) as ViewResult;

            Assert.IsNotNull(result);
        }

        [TestMethod]
        public void Manager_returnView_ApproveFailed()
        {
            ManagerController controller = new ManagerController();

            ViewResult result = controller.ApproveFailed(1) as ViewResult;

            Assert.IsNotNull(result);
        }

        [TestMethod]
        public void Manager_returnView_Cancel()
        {
            ManagerController controller = new ManagerController();

            ViewResult result = controller.Cancel(1) as ViewResult;

            Assert.IsNotNull(result);
        }

    }
}
