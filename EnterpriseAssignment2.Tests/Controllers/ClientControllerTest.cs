﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using EnterpriseAssignment2;
using EnterpriseAssignment2.Controllers;
using EnterpriseAssignment2.Services;
using Moq;
using System.Security.Principal;
using EnterpriseAssignment2.Models;
using System.Threading;
using System.Data.Entity;

namespace EnterpriseAssignment2.Tests.Controllers
{
    [TestClass]
    public class ClientControllerTest
    {
        private Mock<IClientService> service;

        [TestInitialize]
        public void Setup()
        {
            service = new Mock<IClientService>();

            service.Setup(x => x.GetDistrictSet())
                   .Returns(() =>
                   {
                       List<District> data = new List<District>();
                       data.Add(new District { Id = 5, Name = "Sydney" });
                       return data.AsEnumerable<District>();
                   });

            service.Setup(x => x.GetClientTypeSet())
                   .Returns(() =>
                   {
                       List<ClientType> data = new List<ClientType>();
                       data.Add(new ClientType { Id = 1, Name = "Person" });
                       data.Add(new ClientType { Id = 2, Name = "Family" });
                       return data.AsEnumerable<ClientType>();
                   });

            // Setup Mock Service
            service.Setup(x => x.FindClientById(It.IsAny<int>()))
                   .Returns<int>(r =>
                   {
                       // Create a fake Client with full detials
                       return new Client
                       {
                           ClientId = r,
                           Name = "Allen",
                           Type = 1,
                           Location = "Hurstville",
                           DistrictId = 5,
                           Contact = "123321"
                       };
                   });

        }

        [TestMethod]
        public void Client_DetailsPageReturnsClientsByID()
        {
            // Client object for save data generate from mock service
            Client client = null;

            // Setup Mock Service
            service.Setup(x => x.FindClientById(It.IsAny<int>()))
                   .Returns<int>(r =>
                   {
                       // Create a fake Client with client id "r"
                       client = new Client { ClientId = r };
                       return client;
                   });


            // Arrange
            ClientController controller = new ClientController(service.Object);

            // Act
            ViewResult result = controller.Details(5) as ViewResult;

            // Assert
            Assert.IsNotNull(result);
            Assert.IsInstanceOfType(result, typeof(ViewResult));
            Assert.AreEqual(client.ClientId,5);
            Assert.AreEqual(((Client)result.Model).ClientId, 5);
        }

        [TestMethod]
        public void Client_DetailsPage_TestAllClientDetails()
        {

            // Arrange
            ClientController controller = new ClientController(service.Object);

            // Act
            ViewResult result = controller.Details(1) as ViewResult;

            // Assert
            Assert.IsNotNull(result);
            Assert.IsInstanceOfType(result, typeof(ViewResult));
            Assert.AreEqual(((Client)result.Model).ClientId, 1);
            Assert.AreEqual(((Client)result.Model).Name, "Allen");
            Assert.AreEqual(((Client)result.Model).Type, 1);
            Assert.AreEqual(((Client)result.Model).Location, "Hurstville");
            Assert.AreEqual(((Client)result.Model).DistrictId, 5);
            Assert.AreEqual(((Client)result.Model).Contact, "123321");
        }

        
        [TestMethod]
        public void Client_Open_IndexPage()
        {

            Mock<IClientService> service = new Mock<IClientService>();

            // Client List for save data query from mock service
            List<Client> clients = new List<Client>();

            // If you want to put this fake user principal into environment, you can use following way
            //var identity = new GenericIdentity("TestUserName");
            //var principal = new GenericPrincipal(identity, new[] { "SiteEngineer" });
            //Thread.CurrentPrincipal = principal;

            // Setup Mock Service
            service.Setup(x => x.FindClientsByUserDistrict(It.IsAny<GenericPrincipal>()))
                   .Returns<IPrincipal>(r =>
                   {
                       clients.Add(new Client { ClientId = 1, Name = "Allen", Type = 1, Location = "Hurstville", DistrictId = 5 });
                       return clients;
                   });

            // Arrange
            ClientController controller = new ClientController(service.Object);
            
            // Act
            ViewResult result = controller.Index() as ViewResult;


            // Assert
            Assert.IsNotNull(result);
            Assert.IsInstanceOfType(result, typeof(ViewResult));
            Assert.AreEqual(1, clients.Count);
            Assert.AreEqual(1, clients[0].ClientId);
            Assert.AreEqual("Allen", clients[0].Name);
            Assert.AreEqual(1, ((List<Client>)result.Model).Count);
            Assert.AreEqual("Allen", ((List<Client>)result.Model)[0].Name);
        }

        [TestMethod]
        public void Client_ReturnView_CreatePage()
        {
            service.Setup(x => x.GetDistrictByUser(It.IsAny<GenericPrincipal>()))
                   .Returns<IPrincipal>(r =>
                   {
                       List<District> data = new List<District>();
                       data.Add(new District { Id = 5, Name = "Sydney" });
                       return data.AsQueryable();
                   });

            // Arrange
            ClientController controller = new ClientController(service.Object);

            // Act
            ViewResult result = controller.Create() as ViewResult;

            // Assert
            Assert.IsNotNull(result);
        }

        [TestMethod]
        public void Client_CreateClientOnCreatePage()
        {

            Client client = null;

            // If you want to put this fake user principal into environment, you can use following way
            //var identity = new GenericIdentity("TestUserName");
            //var principal = new GenericPrincipal(identity, new[] { "SiteEngineer" });
            //Thread.CurrentPrincipal = principal;

            // Setup Mock Service
            service.Setup(x => x.CreateClient(It.IsAny<Client>()))
                   .Callback<Client>(r => {
                       client = new Client
                       {
                           ClientId = r.ClientId,
                           Name = r.Name,
                           Type = r.Type,
                           Location = r.Location,
                           DistrictId = r.DistrictId,
                           Contact = r.Contact
                       };
                   });
                   

            // Arrange
            ClientController controller = new ClientController(service.Object);

            Client formData = new Client
            {
                ClientId = 5,
                Name = "Allen",
                Type = 1,
                Location = "Hurstville",
                DistrictId = 5,
                Contact = "123321"
            };

            // Act
            RedirectToRouteResult result = controller.Create(formData) as RedirectToRouteResult;

            // Assert
            Assert.IsNotNull(result);
            Assert.IsInstanceOfType(result, typeof(RedirectToRouteResult));
            Assert.AreEqual("Index", result.RouteValues["Action"]);
            Assert.AreEqual(null, result.RouteValues["Controller"]);

        }


    }
}
