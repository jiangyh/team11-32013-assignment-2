﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;
//using System.Threading.Tasks;

//namespace EnterpriseAssignment2.Tests
//{
//    [TestClass]
//    class InterventionLogicTests
//    {
//        public TestContext TestContext { get; set; }

//        ClientLogic _clientLogic;
//        InterventionLogic _interventionLogic;
//        UserLogic _userlogic;
//        InterventionType type1;
//        InterventionType type2;
//        InterventionType type3;

//        [TestInitialize]
//        public void Setup()
//        {
//            _clientLogic = new ClientLogic();
//            _interventionLogic = new InterventionLogic();
//            _userlogic = new UserLogic();
//            type1 = new InterventionType("Hepatitis Avoidance Training", 48, 3000);
//            type2 = new InterventionType("Supply and Install Portable Toilet", 24, 2000);
//            type3 = new InterventionType("Supply and Install Storm-proof Home Kit", 36, 1000);
//        }

//        [TestMethod]
//        public void Create_New_Intervention_By_LoginedAccount_WithHigherApprovable()
//        {
//            ApplicationUser currentUser = new ApplicationUser();
//            currentUser.ApprovableCost = 2000;
//            currentUser.ApprovableLabour = 48;
//            State result = _interventionLogic.CreateNewInterventionTest(currentUser, type3, 36, 1000);
//            Assert.AreEqual(State.Completed, result);
//        }

//        [TestMethod]
//        public void Create_New_Intervention_By_LoginedAccount_WithLowerApprovable()
//        {
//            ApplicationUser currentUser = new ApplicationUser();
//            currentUser.ApprovableCost = 2000;
//            currentUser.ApprovableLabour = 24;
//            State result = _interventionLogic.CreateNewInterventionTest(currentUser, type1, 36, 3000);
//            Assert.AreEqual(State.Proposed, result);
//        }

//        [TestMethod]
//        public void Create_New_Intervention_By_LoginedAccount_WithLowerDefualtApprovable()
//        {
//            ApplicationUser currentUser = new ApplicationUser();
//            currentUser.ApprovableCost = 2000;
//            currentUser.ApprovableLabour = 24;
//            State result = _interventionLogic.CreateNewInterventionTest(currentUser, type1, 12, 1000);
//            Assert.AreEqual(State.Proposed, result);
//        }

//        [TestMethod]
//        public void Create_New_Intervention_By_LoginedAccount_And_ApproveByOtherUser()
//        {
//            ApplicationUser currentUser = new ApplicationUser();
//            currentUser.ApprovableCost = 3000;
//            currentUser.ApprovableLabour = 48;
//            string result = _interventionLogic.ApproveInterventionTest(currentUser, 36, 1000);
//            Assert.AreEqual("Success", result);

//        }
//    }
//}
